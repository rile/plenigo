<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Carbon\Carbon;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Filesystem\Filesystem;

class NasaEpicApi extends Command
{
    protected static $defaultName = 'app:nasa-epic-api';
    protected static $defaultDescription = 'Takes all images of a given day from the NASA EPIC API';

    private $nasaApiKey;
    private $client;
    private $filesystem;

    public function __construct($nasaApiKey, HttpClientInterface $client)
    {
        $this->nasaApiKey = $nasaApiKey;
        $this->client = $client;
        $this->filesystem = new Filesystem();
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            "Starting.....",
        ]);

        try {
            if ($input->getArgument("date")) {
                $date = Carbon::parse($input->getArgument("date"));
            } else {
                $date = Carbon::now();
            }
        } catch (\Exception $e) {
            $output->writeln([
                "<error>Wrong date format</error>"
            ]);

            return Command::FAILURE;
        }

        $destinationFolder = $input->getArgument("destination_folder");
        $destinationFolder = preg_replace("/\.\.\//" ,"", $destinationFolder);

        $response = $this->client->request(
            "GET",
            "https://api.nasa.gov/EPIC/api/natural/date/{$date->toDateString()}?api_key={$this->nasaApiKey}"
        );

        $statusCode = $response->getStatusCode();

        if ($statusCode === 200) {
            $content = $response->toArray();

            ProgressBar::setFormatDefinition('custom', ' %current%/%max% -- %message% <fg=yellow>%filename%</> %status%');
            $progressBar = new ProgressBar($output, count($content));
            $progressBar->setRedrawFrequency(0.1);
            $progressBar->setFormat('custom');

            foreach ($content as $item) {
                $progressBar->setMessage('Fetching...');
                $name = $item["image"] . ".png";

                $source = "https://epic.gsfc.nasa.gov/archive/natural/{$date->format('Y/m/d')}/png/{$name}";
                $destination = "./public/uploads/{$destinationFolder}/{$date->format('Y-m-d')}";

                try {
                    if (!$this->filesystem->exists($destination)) {
                        $this->filesystem->mkdir($destination);
                    }
                    copy($source, $destination."/".$name);    // download and copy the image
                    $progressBar->setMessage("<fg=green>Success</>", "status");
                } catch (\Exception $e) {
                    $progressBar->setMessage( "<fg=red>Failed: {$e->getMessage()}</>", "status");
                }

                $progressBar->setMessage($name, 'filename');
                $progressBar->advance();
                $output->writeln("");
            }
            $progressBar->finish();
        } else {
            $output->writeln([
                "<error>Error getting data from api</error>"
            ]);

            return Command::FAILURE;
        }

        $output->writeln([
            "Done",
        ]);
        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp($this::$defaultDescription)
            ->addArgument('destination_folder', InputArgument::REQUIRED, "Destination folder (default: public/uploads/<current_date>)")
            ->addArgument('date', InputArgument::OPTIONAL, "Date (default: today), YYYY-MM-DD")
        ;
    }
}
